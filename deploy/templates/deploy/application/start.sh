#!/usr/bin/env bash
set -e
echo $NODE_ENV
env
# Run the server (in the foreground)
node_modules/.bin/babel-node --experimental -e "require('./src/server').run();" | node_modules/.bin/bunyan
