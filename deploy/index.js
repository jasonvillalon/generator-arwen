'use strict';
var util = require('util'),
  path = require('path'),
  yeoman = require('yeoman-generator'),
  chalk = require('chalk'),
  shell = require('shelljs');

var restifyDeployGenerator = yeoman.generators.Base.extend({
  init: function(){
    // invoke npm install on finish
    this.on('end', function() {
      console.log(chalk.magenta(this.buildBaseCMD));
      console.log(chalk.magenta(this.buildAppCMD));
      console.log(chalk.magenta(this.startAppCMD));
    });
    // have Yeoman greet the user
    console.log(this.yeoman);

    // replace it with a short and sweet description of your generator
    console.log(chalk.magenta('You\'re using the restify generator.'));
  },
  askForApplicationDetails: function(){
    var done = this.async();

    var prompts = [{
      name: 'nameOfApplicationBuild',
      message: 'What is the name of the application container?',
      default: 'my-api-server'
    },{
      name: 'nodeVersion',
      message: 'What is the node version you want to install?',
      default: 'v0.12.2'
    },{
      name: 'pythonVersion',
      message: 'What is the python version you want to install?',
      default: '2.7.9'
    },{
      name: 'gitRepositorySSH',
      message: 'What is the repository of this project?',
      default: ''
    },{
      name: 'installBase',
      message: 'Do you want to install base?',
      default: false
    },{
      name: 'installApp',
      message: 'Do you want to install application?',
      default: true
    },{
      name: 'port80',
      message: 'What port you want to run your api?',
      default: 9090
    }];

    this.prompt(prompts, function(props) {
      this.nameOfApplicationBuild = props.nameOfApplicationBuild;
      this.nodeVersion = props.nodeVersion;
      this.pythonVersion = props.pythonVersion;
      this.gitRepositorySSH = props.gitRepositorySSH;
      this.installBase = props.installBase;
      this.installApp = props.installApp;
      this.port80 = props.port80;
      done();
    }.bind(this));
  },
  copyApplicationFolder: function(){
    this.mkdir('deploy');
    this.mkdir('deploy/base');
    this.mkdir('deploy/application');
    this.mkdir('deploy/application/ssh');

    this.copy('deploy/application/ssh/id_rsa');
    this.copy('deploy/application/ssh/id_rsa.pub');
    this.template('deploy/application/Dockerfile','deploy/application/Dockerfile');
    this.template('deploy/application/start.sh','deploy/application/start.sh');

    this.template('deploy/base/Dockerfile','deploy/base/Dockerfile');
  },
  startInstallBase: function(){
    this.buildBaseCMD = 'cd deploy/base && sudo docker build -t microcms-api_base:latest .';
    if(this.installBase){
      this.directory('deploy/base');
      console.log(chalk.magenta(this.buildBaseCMD));
      shell.exec(this.buildBaseCMD);
    }
  },
  startBuildApp: function(){
    if(this.installApp){
      this.directory('deploy/application');
      this.buildAppCMD = 'cd deploy/application && sudo docker build -t '+this.nameOfApplicationBuild+' .';
      console.log(chalk.magenta(this.buildAppCMD));
      shell.exec(this.buildAppCMD);
    }
  },
  startInstallApp: function(){
    if(this.installApp){
      this.directory('deploy/application');
      this.startAppCMD = 'cd deploy/application && sudo docker run --name ' + this.nameOfApplicationBuild + '-instance -p '+this.port80+':80  --link postgresql:db --link redis:redis -d -it --restart=always ' + this.nameOfApplicationBuild;
      console.log(chalk.magenta(this.startAppCMD));
      shell.exec(this.startAppCMD);
    }
  }
});

module.exports = restifyDeployGenerator;
