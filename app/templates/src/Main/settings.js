module.exports = {
  Name: "Main",
  Description: "Main",
  Repository: "",
  Version: "0.0.0",
  AtomicDeps: [],
  config: {
    // Name of the application
    name: "<%= appName %>",
    // Connection information for the database (postgresql) server
    db: {
      name: "<%= dbName %>",
      host: "<%= dbHost %>",
      user: "<%= dbUser %>",
      password: "<%= dbPass %>",
      port: <%= dbPort %>
    }
  }
};
