module.exports = {
  Name: "<%= classifiedComponentName %>",
  Description: "<%= humanizedComponentName %>",
  Repository: "<%= componentRepository %>",
  Version: "0.0.0",
  AtomicDeps: <%= dependencies %>
};
