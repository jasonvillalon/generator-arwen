var path = require("path"),
    _ = require("lodash"),
  shell = require("shelljs");

var npmInstall = function(atomic, isLocalComponentOnly) {
  var root = this.isComponentDep ? "./../../" : "./"
  console.log(this.isComponentDep)
  console.log(root)
  var packages = require(path.resolve(root + "package"));
  _.each(atomic.dependencies, function(version, package) {
    var installed = !!packages.dependencies[package]
    if (!installed) {
      shell.exec("npm install --save " + package + (version != "" ? "@" + version : ""));
      // set as true (meaning installed)
      atomic.dependencies[package] = version;
    }
  });
};

module.exports = npmInstall;
